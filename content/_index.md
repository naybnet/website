+++
title = ""
date = 2019-10-11
show_date = false
+++

I am a Software Engineer living in Lyon, France. I am interested in data-centric applications tackling climate change.

Have a look at my [now](/page/now) page to see what I am up too these days.

You may find my resume in French [here](/bolnet_cv_fr_20191008_anon.pdf) and my [projects](/page/projects) page is currently under construction...