+++
title = "Now"
date = 2020-09-06
show_date = false
+++

I am currently the lead software engineer at [Vizcab](https://vizcab.io) with the goal to 
catalyze the carbon transition in the construction sector by providing advanced LCA software and data-driven analytics.

Before that I was building data pipelines from governmental data in order to model the population and employment makeup of cities.

More generally, I take a keen interest in projects that aim at reducing human CO₂ emissions and/or fostering carbon capture. Whether it is in agriculture, construction or mobility, there is a lot to do !

I am also a volunteer with [Avenir Climatique](https://avenirclimatique.org), an organization that teaches students and professionals about energy and climate.


[What is a now page anyway?](https://nownownow.com/about)